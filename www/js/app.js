

var app = angular.module('starter', ['ionic'])

app.controller('lyftCtrl',function($scope){
 $scope.stories=[
   {id:'1',title:'My data 1',desc:'Description of my data'
    },{
   id:'2',title:'My data 2',desc:'Description of my data'
   },{
  id:'3',title:'My data 3',desc:'Description of my data'
   }];
});
app.controller('p1Ctrl',function($scope,$state){
$scope.noteId=$state.params.noteId;
});
 app.config(function($stateProvider,$urlRouterProvider){
  $stateProvider.state('list',{
    url:'/list',templateUrl:'templates/list.html'
  });
  $stateProvider.state('p1',{
    url:'/p1/:noteId',templateUrl:'templates/p1.html'
  })
  $urlRouterProvider.otherwise('/list');
});

app.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
});
